/*
 * parsers.cpp
 *
 *  Created on: 17 марта 2015
 *      Author: strangeman
 */

#include "parsers.hpp"

namespace Parsers {

	bool IsEOL ( std::istream & _stream ) {
		char temp;
		if ( ! _stream.eof() )
			if ( (temp = _stream.get()) != '\n' ) {
				_stream.unget();
				return false;
			}

		return true;
	}

	bool IgnoreUntilEOL ( std::istream & _stream ) {
		bool isFoundSomethingElse = false;

		if (  _stream.eof() || ( _stream.get()) == '\n'  )
			return isFoundSomethingElse;

		isFoundSomethingElse = true;

		std::cin.ignore();

		return isFoundSomethingElse;
	}

	void IgnoreNumbers ( std::istream & _stream ) {
		char temp;
		do {
			if (_stream.eof()) {
				break;
			}
			temp = _stream.get();
		}
		while ( ((temp >= '0') && (temp <= '9')) || ( temp == '-' ) || (temp == '.') );

		_stream.unget();
	}

	bool FindNumberInLineAndEatPrecedingSymbols ( std::istream & _stream ) {
		char temp;
		do {
			if ( _stream.eof() ) {
				return false;
			}

			temp = _stream.get();

			if ( temp == '\n' ) {
				_stream.unget();
				return false;
			}

		}
		while ( ! (((temp >= '0') && (temp <= '9')) || ( temp == '-' )) );

		_stream.unget();
		return true;
	}

	//Пытается получить из потока следующий int, возвращает:
	// -1, если число в строке не найдено;
	// 1, если перед int встретились какие-либо символы;
	// 0 - во всех остальных случаях.
	int GetNextInt (int & _value, std::istream & _stream) {
		char temp = _stream.get();
		int status;
		_stream.unget();
		if ( ! (((temp >= '0') && (temp <= '9')) || ( temp == '-' ) ) ) {

			if ( FindNumberInLineAndEatPrecedingSymbols ( _stream ) )
				status = 1;

			else
				return -1;

		}

		else
			status = 0;


		_stream >> _value;
		IgnoreNumbers ( _stream );
		return status;
	}

	bool GetNextReal (double & _value, std::istream & _stream) {
		if ( FindNumberInLineAndEatPrecedingSymbols ( _stream ) ) {
			_stream >> _value;
			return true;
		}
		return false;
	}

	void ReadWord( CharVector::ChVector & _wordVector, std::istream & _input ) {
		char temp = _input.get();
		while ( isalpha( temp ) != 0 ) {
			if ( _input.eof() )
				return;

			CharVector::PushBack(_wordVector, temp);
			temp = _input.get();
		}

		_input.unget();
	}

	void ProxyNotAlphas( std::istream & _input,  std::ostream & _output ) {
		char temp = _input.get();
		while ( isalpha( temp ) == 0 ) {
			if ( _input.eof() )
				return;

			_output << temp;
			temp = _input.get();
		}

		_input.unget();
	}
} //end of parsers namespace
