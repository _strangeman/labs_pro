#include "real_vector.hpp"

#include <cstring>
#include <iostream>
#include <cassert>

namespace RealVector {
    void Init ( RVector & _vector, int _allocatedSize )
    {
        _vector.m_pData      = new double[ _allocatedSize ];
        _vector.m_nAllocated =  _allocatedSize;
        _vector.m_nUsed      = 0;
    }


    void Destroy ( RVector & _vector )
    {
        delete[] _vector.m_pData;
    }


    void Clear ( RVector & _vector )
    {
        _vector.m_nUsed = 0;
    }


    bool IsEmpty ( const RVector & _vector )
    {
        return _vector.m_nUsed == 0;
    }


    void Grow ( RVector & _vector )
    {
        int nAllocatedNew = _vector.m_nAllocated * 2;
        double * pNewData = new double[ nAllocatedNew ];

        memcpy( pNewData, _vector.m_pData, sizeof( double ) * _vector.m_nAllocated );

        delete[] _vector.m_pData;
        _vector.m_pData = pNewData;

        _vector.m_nAllocated = nAllocatedNew;
    }


    void PushBack ( RVector & _vector, double _data )
    {
        if ( _vector.m_nUsed == _vector.m_nAllocated )
            Grow( _vector );

        _vector.m_pData[ _vector.m_nUsed++ ] = _data;
    }



    void RealVectorPopBack ( RVector & _vector )
    {
        assert( ! IsEmpty( _vector ) );

        -- _vector.m_nUsed;
    }


    void RealVectorRead ( RVector & _vector, std::istream & _stream )
    {
        while ( true )
        {
            double temp;
            _stream >> temp;
            if ( _stream )
                PushBack( _vector, temp );
            else
                break;
        }    
    }


    void RealVectorReadTillZero ( RVector & _vector, std::istream & _stream )
    {
        while ( true )
        {
            double temp;
            _stream >> temp;
            if ( _stream && temp != 0 )
                PushBack( _vector, temp );
            else
                break;
        }
    }


    void RealVectorPrint ( const RVector & _vector, std::ostream & _stream, char _sep )
    {
        for ( int i = 0; i < _vector.m_nUsed; i++ )
            _stream << _vector.m_pData[ i ] << _sep;
    }


    void RealVectorInsertAt ( RVector & _vector, int _position, double _data )
    {
        assert( _position >= 0 && _position < _vector.m_nUsed );

        int newUsed = _vector.m_nUsed + 1;
        if ( newUsed > _vector.m_nAllocated )
            Grow( _vector );

        for ( int i = _vector.m_nUsed; i > _position; i-- )
            _vector.m_pData[ i ] = _vector.m_pData[ i - 1];

        _vector.m_pData[ _position ] = _data;

        _vector.m_nUsed = newUsed;
    }


    void RealVectorDeleteAt ( RVector & _vector, int _position )
    {
        assert( _position >= 0 && _position < _vector.m_nUsed );

        for ( int i = _position + 1; i < _vector.m_nUsed; i++ )
            _vector.m_pData[ i - 1 ] = _vector.m_pData[ i ];

        -- _vector.m_nUsed;
    }

} //end of vectors namespace
