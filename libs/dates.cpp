#include "dates.h"

int getRandNumber() {
	return (rand() % 1000);
}

bool isLeapYear(int _year) {
	if ((_year % 4) == 0) {
		if (((_year % 100) > 0) || ((_year % 400) == 0)) {
			return true;
		}
	}
	//else do this:
	return false;
}

int getMaxDays(MONTH _m, int _year) {
	int max = maxDaysInMonth[_m];
	if ((_m == FEBRUARY) && isLeapYear(_year)) {
		max++;
	}
	return max;
}

void printDate (Date & _date) {
	printf("%02d.%02d.%04d", _date.day, _date.month, _date.year);
}

void fillRandDate(Date & _date) {
	_date.year = 2015;
	_date.month = static_cast<MONTH>(rand() % 12 + 1);
	_date.day = rand() % (getMaxDays(_date.month, _date.year)) + 1;
}

void plusDay (Date & _date) {
	if (_date.day == (getMaxDays(_date.month, _date.year))) {
		if (_date.month == DECEMBER) {
			_date.year++;
			_date.month = JANUARY;
		}
		_date.day = 1;
		_date.month = static_cast<MONTH>(_date.month + 1);
	}
	else {
		_date.day++;
	}
}

long getDateInDaysWithoutYears(Date _d) {
	return (_d.day + _d.month * getMaxDays(_d.month, _d.year));
}

int getDateDiffInDays(Date _d1, Date _d2) {
	return (getDateInDaysWithoutYears(_d1) - getDateInDaysWithoutYears(_d2));
}

int getModuleDateDiff(Date _d1, Date _d2) {
	int diff = getDateDiffInDays(_d1, _d2);
	diff = (diff < 0) ? (diff * -1) : diff;
	return diff;
}
