/*
 * parsers.hpp
 *
 *  Created on: 17 марта 2015
 *      Author: strangeman
 */

#ifndef PARSERS_HPP_
#define PARSERS_HPP_

#include <iostream>
#include "char_vector.hpp"

namespace Parsers {
	void IgnoreNumbers ( std::istream & _stream );

	bool IsEOL ( std::istream & _stream );

	bool IgnoreUntilEOL ( std::istream & _stream );

	bool FoundNumberInLine (std::istream & _stream);

	//Возвращает статус, записывает значение в _value
	int GetNextInt (int & _value, std::istream & _stream);

	bool GetNextReal (double & _value, std::istream & _stream);

	void ReadWord( CharVector::ChVector & _wordVector, std::istream & _input );

	void ProxyNotAlphas( std::istream & _input,  std::ostream & _output );
} //end of Parsers namespace

#endif /* PARSERS_HPP_ */
