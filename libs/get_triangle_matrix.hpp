/*
 * get_triangle_matrix.hpp
 *
 *  Created on: 11 апр. 2015
 *      Author: strangeman
 */

#ifndef GET_TRIANGLE_MATRIX_HPP_
#define GET_TRIANGLE_MATRIX_HPP_

#include "real_matrix.hpp"

namespace RealMatrix {

	void MakeTriangleBareiss (RMatrix & _matrix);

} //end of RealMatrix namespace

#endif /* GET_TRIANGLE_MATRIX_HPP_ */
