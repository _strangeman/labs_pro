#include "integer_vector.hpp"

#include <cstring>
#include <iostream>
#include <cassert>

namespace IntegerVector {
    void Init ( IntVector & _vector, int _allocatedSize )
    {
        _vector.m_pData      = new int[ _allocatedSize ];
        _vector.m_nAllocated =  _allocatedSize;
        _vector.m_nUsed      = 0;
    }


    void Destroy ( IntVector & _vector )
    {
        delete[] _vector.m_pData;
    }


    void Clear ( IntVector & _vector )
    {
        _vector.m_nUsed = 0;
    }


    bool IsEmpty ( const IntVector & _vector )
    {
        return _vector.m_nUsed == 0;
    }


    void Grow ( IntVector & _vector )
    {
        int nAllocatedNew = _vector.m_nAllocated * 2;
        int * pNewData = new int[ nAllocatedNew ];

        memcpy( pNewData, _vector.m_pData, sizeof( int ) * _vector.m_nAllocated );

        delete[] _vector.m_pData;
        _vector.m_pData = pNewData;

        _vector.m_nAllocated = nAllocatedNew;
    }


    void PushBack ( IntVector & _vector, int _data )
    {
        if ( _vector.m_nUsed == _vector.m_nAllocated )
            Grow( _vector );

        _vector.m_pData[ _vector.m_nUsed++ ] = _data;
    }



    void PopBack ( IntVector & _vector )
    {
        assert( ! IsEmpty( _vector ) );

        -- _vector.m_nUsed;
    }


    void Read ( IntVector & _vector, std::istream & _stream )
    {
        while ( true )
        {
            int temp;
            _stream >> temp;
            if ( _stream )
                PushBack( _vector, temp );
            else
                break;
        }    
    }


    void ReadTillZero ( IntVector & _vector, std::istream & _stream )
    {
        while ( true )
        {
            int temp;
            _stream >> temp;
            if ( _stream && temp != 0 )
                PushBack( _vector, temp );
            else
                break;
        }
    }


    void Print ( const IntVector & _vector, std::ostream & _stream, char _sep )
    {
        for ( int i = 0; i < _vector.m_nUsed; i++ )
            _stream << _vector.m_pData[ i ] << _sep;
    }


    void InsertAt ( IntVector & _vector, int _position, int _data )
    {
        assert( _position >= 0 && _position < _vector.m_nUsed );

        int newUsed = _vector.m_nUsed + 1;
        if ( newUsed > _vector.m_nAllocated )
            Grow( _vector );

        for ( int i = _vector.m_nUsed; i > _position; i-- )
            _vector.m_pData[ i ] = _vector.m_pData[ i - 1];

        _vector.m_pData[ _position ] = _data;

        _vector.m_nUsed = newUsed;
    }


    void DeleteAt ( IntVector & _vector, int _position )
    {
        assert( _position >= 0 && _position < _vector.m_nUsed );

        for ( int i = _position + 1; i < _vector.m_nUsed; i++ )
            _vector.m_pData[ i - 1 ] = _vector.m_pData[ i ];

        -- _vector.m_nUsed;
    }

} //end of vectors namespace
