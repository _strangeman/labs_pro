/*
 * student_record.hpp
 *
 *  Created on: 1 мая 2015
 *      Author: strangeman
 */

#include "student_record.hpp"
#include "parsers.hpp"
#include <cassert>
#include <cstring>

namespace StudentRecord {

	void ReadGrades (IntegerVector::IntVector & _vector, std::istream & _stream) {
		int temp;
		while ( Parsers::GetNextInt (temp, _stream) != -1)
			IntegerVector::PushBack (_vector, temp);
	}

	void RecordInit (Record & _record) {
		assert (! _record.m_isInitiated);
		CharVector::Init (_record.m_name);
		IntegerVector::Init (_record.m_grades);
		_record.m_isInitiated = true;
	}

	void RecordDestroy (Record & _record) {
		assert (_record.m_isInitiated);
		CharVector::Destroy (_record.m_name);
		IntegerVector::Destroy (_record.m_grades);
		_record.m_isInitiated = false;
	}

	void RecordRead (Record & _record, std::istream & _stream) {
		assert (_record.m_isInitiated);
		CharVector::GetUntil (_record.m_name, _stream, "\t0123456789");
		ReadGrades (_record.m_grades, _stream);
		Parsers::IgnoreUntilEOL (_stream);
	}

	void RecordPrint (const Record & _record, std::ostream & _stream) {
		assert (_record.m_isInitiated);
		CharVector::Print (_record.m_name, _stream, '\t');
		IntegerVector::Print (_record.m_grades, _stream, ' ');
		_stream << "\n";
	}

	void RecordClear (Record & _record) {
		CharVector::Clear (_record.m_name);
		IntegerVector::Clear (_record.m_grades);
	}

	void RecordCopy (Record & _dest, const Record & _source) {
		assert (_dest.m_isInitiated);

		//Копируем имя
		CharVector::CopyAndReplace (_dest.m_name, _source.m_name);

		//Копируем оценки
		if (_source.m_grades.m_nUsed > _dest.m_grades.m_nUsed) {
			IntegerVector::Destroy (_dest.m_grades);
	        int nAllocatedNew = _source.m_grades.m_nAllocated;
	        _dest.m_grades.m_pData = new int[ nAllocatedNew ];
	        _dest.m_grades.m_nAllocated = nAllocatedNew;
		}

		memcpy( _dest.m_grades.m_pData, _source.m_grades.m_pData, sizeof( int ) * _source.m_grades.m_nAllocated );

		_dest.m_grades.m_nUsed = _source.m_grades.m_nUsed;
		//...и заканчиваем копировать тут
	}


} //end of StudentRecord namespace
