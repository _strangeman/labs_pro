/*
 * fix_on_the_fly.hpp
 *
 *  Created on: May 31, 2015
 *      Author: betatester
 */

#ifndef FIX_ON_THE_FLY_HPP_
#define FIX_ON_THE_FLY_HPP_

#include "dict.hpp"

void FixOnTheFly( const Dict::Dictionary & _dict, std::istream & _input,  std::ostream & _output);

#endif /* FIX_ON_THE_FLY_HPP_ */
