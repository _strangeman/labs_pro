/*
 * main.cpp
 *
 *  Created on: May 31, 2015
 *      Author: betatester
 */

// Простая проверка орфографии
/*
 * 1. Считать словарь (текстовый файл)
 * 2. Считать входной файл
 * 3. Каждое слово проверять по словарю. Если в словаре найдено слово,
 * которое отличается на один символ - заменять на вариант из словаря.
 * 4. Отредактированная версия сохраняется в новый файл.
 * 4+ Если успею - реализую исправление в исходном файле.
 */

#include <fstream>

#include "print_message.hpp"
#include "fix_on_the_fly.hpp"
#include <string.h>

int main (int _argc, char** _argv) {
	// Проверяем количество аргументов
	if (_argc < 4) {
		PrintMessage(ERR_FILE_NON_SPECIFIED, std::cout, static_cast<FILE_ID>(_argc - 1));
		return -1;
	}

	// Пытаемся открыть файлы на чтение и запись
	std::ifstream dictFile(_argv[1]);
	std::ifstream sourceFile(_argv[2]);
	std::ofstream resultFile(_argv[3]);

	// Проверяем, удалось ли получить доступ к файлам и открыть их
	if ( ! dictFile.is_open() ) {
		PrintMessage(ERR_BAD_FILE, std::cout, FIRST_SOURCE_FILE);
		return -2;
	}

	if ( ! sourceFile.is_open() ) {
		PrintMessage(ERR_BAD_FILE, std::cout, SECOND_SOURCE_FILE);
		return -2;
	}

	if ( ! resultFile.is_open() ) {
		PrintMessage(ERR_BAD_FILE, std::cout, RESULT_FILE);
		return -2;
	}

	//Считываем словарь
	Dict::Dictionary dict;
	Dict::Init( dict );
	Dict::Read( dict, dictFile );

	// ...и закрываем файл со словарем
	dictFile.close();

	// Сохраняем результат в файл resultFile,...
	FixOnTheFly( dict, sourceFile, resultFile );

	// ...закрываем текстовые файлы...
	sourceFile.close();
	resultFile.close();

	// ...и выгружаем словарь из памяти
	Dict::Destroy( dict );

	std::cout << "\nFinished";
	return 0;
}
