/*
 * marsh.h
 *
 *  Created on: 14 марта 2015
 *      Author: strangeman
 */

#ifndef MARSH_H_
#define MARSH_H_

#include <cassert>
#include <string.h>
#include "char_vector.hpp"

namespace MarshSpace {
struct MARSH {
	CharVector::ChVector m_source, m_dest;
	int m_routeNum;
};

struct marshArray {
	MARSH * m_pMarsh = nullptr;
	int m_nMax = 0;
	int m_nUsed = 0;
};

void initMarsh(MARSH & _m);

void initMarshArray(marshArray & _arr, int _size);

void copyMarsh(MARSH & _to, MARSH & _from);

void shiftMarshes(marshArray & _arr, int _startIndex);

void getMarsh(MARSH & _m, std::istream & _stream);

void placeMarshToSortedArray(MARSH & _m, marshArray & _arr);

void fillMarshArray(marshArray & _arr, std::istream & _stream = std::cin);

void printMarsh(MARSH & _m, std::ostream & _stream = std::cout);

void printMarshArray(marshArray & _arr, std::ostream & _stream = std::cout);

void printMarshPlaceMatch(marshArray & _arr, char * _place, std::ostream & _stream = std::cout);

void destroyMarsh(MARSH & _m);

void destroyMarshArray(marshArray & _arr);
}//end of MarshSpace namespace

#endif /* MARSH_H_ */
