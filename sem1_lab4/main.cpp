#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void randInit() {
	time_t currentTime;
	time( & currentTime );
	srand( currentTime );
}

int getRand() {
	return (rand() % 20 - 10);
}

void swap(int* elementAdress1, int* elementAdress2) {
	int temp;
	temp = *elementAdress1;
	*elementAdress1 = *elementAdress2;
	*elementAdress2 = temp;
}

void fillAndInitMatrix(int** pMatrix, const int numOfRows, const int numOfColumns) {
	for (int i = 0; i < numOfRows; i++) {
		int* pRow = new int [numOfColumns];
		for (int j = 0; j < numOfColumns; j++) {
			pRow[j] = getRand();
		}
		pMatrix[i] = pRow;
	}
}

void printMatrix(int** pMatrix, int numOfRows, int numOfColumns) {
	for(int i = 0; i < numOfRows; i++) {
		for(int j = 0; j < numOfColumns; j++) {
			printf ("%d\t", pMatrix[i][j]);
		}
		printf("\n");
	}
}

void squareMatrixManipulations(int** pMatrix, int lengthOfRow) {
	for(int i = 0; i < lengthOfRow; i++) {
		int* addressOfMin = &(pMatrix[i][i]);
		for(int j = i + 1; j < lengthOfRow; j++){
			if (*(addressOfMin) > pMatrix[j][j]) {
				addressOfMin = &(pMatrix[j][j]);
			}
		}
		swap(addressOfMin, &(pMatrix[i][i]));
	}
	swap(&pMatrix[0][0], &pMatrix[lengthOfRow - 1][lengthOfRow - 1]);
}

void releaseMemory(int** pMatrix, int numOfRows, int numOfColumns) {
	for (int i = 0; i < numOfRows; i++) {
		delete[] pMatrix[i];
	}
	delete[] pMatrix;
}

int main() {
	randInit();
	int matrixSize;
	printf("Input size of a square matrix, please: ");
	scanf("%d", &matrixSize);
	int** matrix = new int* [matrixSize];
	fillAndInitMatrix(matrix, matrixSize, matrixSize);
	printf("OK. Your source matrix is:\n");
	printMatrix(matrix, matrixSize, matrixSize);
	squareMatrixManipulations(matrix, matrixSize);
	printf("After manipulations this matrix received:\n");
	printMatrix(matrix, matrixSize, matrixSize);
	releaseMemory(matrix, matrixSize, matrixSize);
	return 0;
}
