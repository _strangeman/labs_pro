#include <stdlib.h>
#include <time.h>
#include <stdio.h>

struct Time {
	int hour, minute;
};

struct Transport {
	int number, route;
	Time arriveTime, shippingTime;
};

struct Schedule {
	Transport * m_units;
	int m_number;
};

int getRandNumber() {
	return (rand() % 1000);
}

int getRandHour() {
	return (rand() % 12 + 9);
}

int getRandMinute() {
	return ((rand() % 6) * 10);
}

void initSchedule(Schedule & _sched, int _n = 10) {
	_sched.m_number = _n;
	_sched.m_units = new Transport[_sched.m_number];
}

void fillSchedule(Schedule & _sched) {
	for (int i = 0; i < _sched.m_number; i++) {
		_sched.m_units[i].arriveTime.hour = getRandHour();
		_sched.m_units[i].arriveTime.minute = getRandMinute();
		_sched.m_units[i].shippingTime.hour = getRandHour();
		_sched.m_units[i].shippingTime.minute = getRandMinute();
		_sched.m_units[i].number = i + 1;
		_sched.m_units[i].route = getRandNumber();
	}
}

void printScheduleUnit(Schedule & _sched, int _i) {
	printf(
			"%d\t%3d\t\t%02d:%02d\t\t%02d:%02d\n",
			_sched.m_units[_i].number, _sched.m_units[_i].route,
			_sched.m_units[_i].arriveTime.hour, _sched.m_units[_i].arriveTime.minute,
			_sched.m_units[_i].shippingTime.hour, _sched.m_units[_i].shippingTime.minute
			);
}

void printSchedule(Schedule & _sched) {
	printf("Full schedule\nNumber\tRoute number\tArrive time\tShipping time\n");

	for(int i = 0; i < _sched.m_number; i++) {
		printScheduleUnit(_sched, i);
	}
	printf("\n");
}

void printByAriveTime(Schedule & _sched, Time t) {
	for(int i = 0; i < _sched.m_number; i++) {
		if ((_sched.m_units[i].arriveTime.hour == t.hour) && (_sched.m_units[i].arriveTime.minute == t.minute)) {
			printScheduleUnit(_sched, i);
		}
	}
}


void destroySchedule(Schedule & _sched) {
	delete [] _sched.m_units;
	_sched.m_units = nullptr;
}

int main() {
	time_t currentTime;
	time(&currentTime);
	srand(currentTime);

	Schedule sched;
	Time targetArriveTime;

	initSchedule(sched, 10);
	fillSchedule(sched);

	printSchedule(sched);

	printf("Input arrive time: ");

	scanf("%d:%d", &targetArriveTime.hour, &targetArriveTime.minute);

	printf("Number\tRoute number\tArrive time\tShipping time\n");

	printByAriveTime(sched, targetArriveTime);

	destroySchedule(sched);

	return 0;
}
