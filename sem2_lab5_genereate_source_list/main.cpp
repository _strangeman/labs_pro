#include <fstream>
#include "student_record_vector.hpp"
#include <ctime>
#include <cstdlib>
#include "surnames.h"
#include <cassert>

void FillRandomRecord (StudentRecord::Record & _record) {
	StudentRecord::RecordInit (_record);

	for (int i = 0; i < _record.m_grades.m_nAllocated; i++)
		IntegerVector::PushBack(_record.m_grades, rand() % 26 + 75 - rand() % 21);

	CharVector::CopyAndReplace(_record.m_name, surnames[rand() % 100]);
}

void FillRandomRecVector (StudentRecord::RecVector & _vector, int _nOfRecords) {
	assert (_nOfRecords <= _vector.m_nAllocated);

    for ( int i = 0; i < _nOfRecords; i++ )
    	FillRandomRecord ( _vector.m_pData[i] );

    _vector.m_nUsed = _nOfRecords;
}

int main (int _argc, char** _argv) {
	srand (time (NULL));

	// Проверяем количество аргументов
	if (_argc < 2) {
		std::cout << "You must to specify a path to file with information about the students as the first argument. But you didn't it.\nExiting...\n";
		return -1;
	}

	// Пытаемся открыть файлы на чтение и запись
	std::ofstream destFile(_argv[1]);

	// Проверяем, удалось ли получить доступ к файлам и открыть их
	if ( ! destFile.is_open() ) {
		std::cout << "Unable to open your dest file. Does it exist?\nExiting...\n";
		return -2;
	}

	std::cout << "How many records must the destination list contain? Input here: ";

	int nOfRecords;
	std::cin >> nOfRecords;

	StudentRecord::RecVector list;
	StudentRecord::RecVectorInit (list, nOfRecords);

	FillRandomRecVector(list, nOfRecords);

	StudentRecord::RecVectorPrint (list, destFile);

	StudentRecord::RecVectorDestroy (list);

	destFile.close();

	std::cout << "\nFinished\n";
	return 0;
}
